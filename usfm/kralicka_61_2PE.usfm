\id 2PE
\mt1 II Petrova
\c 1
\v 1 Šimon Petr, služebník a apoštol Ježíše Krista, těm, kteříž spolu s námi zaroveň drahé dosáhli víry, pro spravedlnost Boha našeho a spasitele Jezukrista:
\v 2 Milost vám a pokoj rozmnožen buď skrze známost Boha a Ježíše Pána našeho.
\v 3 Jakož nám od jeho Božské moci všecko, což k životu a ku pobožnosti \add náleželo\add*, darováno jest, skrze známost toho, kterýž povolal nás k slávě a k ctnosti;
\v 4 Pročež veliká nám a drahá zaslíbení dána jsou, abyste skrze ně Božského přirození účastni učiněni byli, utekše porušení toho, kteréž jest na světě v žádostech.
\v 5 Na to tedy samo všecku snažnost vynaložíce, přičinějte k víře své ctnost, a k ctnosti umění,
\v 6 K umění pak zdrželivost, a k zdrželivosti trpělivost, k trpělivosti pak pobožnost,
\v 7 Ku pobožnosti pak bratrstva milování, a k milování bratrstva lásku.
\v 8 Ty zajisté věci když budou při vás a rozhojní se, ne prázdné, ani neužitečné postaví vás v známosti Pána našeho Jezukrista.
\v 9 Nebo při komž není těchto věcí, slepýť jest, toho, což vzdáleno jest, nevida, zapomenuv se na očištění svých starých hříchů.
\v 10 Protož raději, bratří, snažujte se povolání a vyvolení své upevňovati; nebo to činíce, nepadnete nikdy.
\v 11 Takť zajisté hojné způsobeno vám bude vjití k věčnému království Pána našeho a spasitele Jezukrista.
\v 12 Protož nezanedbámť vždycky vám připomínati těch věcí, ačkoli umělí i utvrzení jste v přítomné pravdě.
\v 13 Neboť to mám za spravedlivé, dokudž jsem v tomto stánku, abych vás probuzoval napomínáním,
\v 14 Věda, že brzké jest složení stánku mého, jakož mi i Pán náš Ježíš Kristus oznámil.
\v 15 Přičinímť se tedy všelijak o to, abyste vy po odchodu mém na ty věci rozpomínati se mohli.
\v 16 Nebo ne nějakých vtipně složených básní následujíce, známu učinili jsme vám Pána našeho Jezukrista moc a příchod, ale jakožto ti, kteříž jsme očima svýma viděli jeho velebnost.
\v 17 Přijalť zajisté od Boha Otce čest a slávu, když se stal k němu hlas takový od velebné slávy: Tentoť jest ten můj milý Syn, v němž mi se zalíbilo.
\v 18 A ten hlas my jsme slyšeli s nebe pošlý, s ním byvše na oné hoře svaté.
\v 19 A mámeť přepevnou řeč prorockou, kteréžto že šetříte jako svíce v temném místě svítící, dobře činíte, až by se den rozednil, a dennice vzešla v srdcích vašich,
\v 20 Toto nejprvé znajíce, že žádného proroctví písma výklad nezáleží na rozumu lidském.
\v 21 Nebo nikdy z lidské vůle nepošlo proroctví, ale Duchem svatým puzeni byvše, mluvili svatí Boží lidé.
\c 2
\v 1 Bývali pak i falešní proroci v lidu, jakož i mezi vámi budou falešní učitelé, kteříž uvedou sekty zatracení, i toho Pána, kterýž je vykoupil, zapírajíce, uvodíce na sebe rychlé zahynutí.
\v 2 A mnozí následovati budou jejich zahynutí, skrze něž cesta pravdy bude v porouhání dávána.
\v 3 A lakomě skrze vymyšlené řeči vámi kupčiti budou; kterýchž odsouzení již dávno nemešká, a zahynutí jejich nespí.
\v 4 Nebo poněvadžť Bůh andělům, kteříž zhřešili, neodpustil, ale strhna je do žaláře, řetězům mrákoty oddal, aby k odsouzení chováni byli,
\v 5 I prvnímu světu neodpustil, ale \add sama\add* osmého Noé, kazatele spravedlnosti, zachoval, když potopu na svět bezbožníků uvedl,
\v 6 A města Sodomských a Gomorských v popel obrátiv, podvrácením potupil, příklad budoucím bezbožníkům ukázav,
\v 7 A spravedlivého Lota, těch nešlechetníků prostopašným obcováním ztrápeného, vytrhl;
\v 8 Ten zajisté spravedlivý, bydliv mezi nimi, den ode dne hleděním i slyšením spravedlivou duši nešlechetnými \add jejich\add* skutky trápil:
\v 9 Umíť Pán pobožné z pokušení vytrhnouti, nepravých pak ke dni soudu potrestaných dochovati,
\v 10 A zvláště těch, kteříž po těle v žádosti nečisté chodí, a vrchností pohrdají, smělí, sobě se zalibující, neostýchají se důstojnostem porouhati;
\v 11 Ješto andělé, jsouce větší v síle a v moci nevynášejí proti nim přede Pánem potupného soudu.
\v 12 Tito pak jako nerozumná hovada, kteráž za přirozením jdou, zplozená k zjímání a k zahynutí, tomu, čemuž nerozumějí, rouhajíce se, v tom svém porušení zahynou,
\v 13 \add A\add* odplatu nepravosti ponesou, jakožto ti, kteříž sobě za rozkoš položili, aby se na každý den v líbostech svých kochali, poškvrny a mrzkosti, ti, kteříž s vámi hodujíce, v svých lstech se kochají,
\v 14 Oči majíce plné cizoložstva, a bez přestání hřešící, přeluzujíce duše neustavičné, srdce majíce vycvičené v lakomství, synové zlořečenství.
\v 15 Kteříž opustivše cestu přímou, zbloudili, následujíce cesty Balámovy \add syna\add* Bozorova, kterýž mzdu nepravosti zamiloval.
\v 16 Ale měl, od koho by pokárán byl pro svůj výstupek. \add Nebo\add* jhu poddaná oslice němá, člověčím hlasem promluvivši, zbránila nemoudrosti proroka.
\v 17 Tiť jsou studnice bez vody, mlhy vichrem zbouřené, jimžto mrákota tmy schována jest na věčnost.
\v 18 Nebo přepyšně marné věci vypravujíce, žádostmi těla \add a\add* chlipnostmi loudí ty, kteříž byli v pravdě utekli od těch, jenž bludu obcují,
\v 19 Slibujíce jim svobodu, ješto sami jsou služebníci porušení, poněvadž, od kohož kdo jest přemožen, tomu jest i v službu podroben.
\v 20 Jestliže by pak ti, kteříž ušli poškvrn světa, skrze známost Pána a spasitele Jezukrista, opět zase v to zapleteni jsouce, přemoženi byli, učiněn jest poslední způsob jejich horší nežli první.
\v 21 Lépe by zajisté jim bylo nepoznávati cesty spravedlnosti, nežli po nabytí známosti odvrátiti se od vydaného jim svatého přikázaní.
\v 22 Ale přihodilo se jim to, což se v přísloví pravém říkává: Pes navrátil se k vývratku svému, a svině umytá do kaliště bláta.
\c 3
\v 1 Nejmilejší, již toto druhý list vám píši, jimižto vzbuzuji skrze napomínání vaši upřímou mysl,
\v 2 Abyste pamatovali na slova předpověděná od svatých proroků, a na přikázaní \add vydané\add* od nás apoštolů Pána a spasitele,
\v 3 Toto nejprvé vědouce, žeť přijdou v posledních dnech posměvači, podlé svých vlastních žádostí chodící,
\v 4 A říkající: Kdež jest to zaslibování o příchodu jeho? Nebo jakž otcové zesnuli, všecko tak trvá od počátku stvoření.
\v 5 Tohoť zajisté z úmysla věděti nechtí, že nebesa již dávno slovem Božím byla učiněna, i země z vody a na vodě upevněna.
\v 6 Pročež onen první svět vodou jsa zatopen, zahynul.
\v 7 Ta pak nebesa, kteráž nyní jsou, i země, týmž slovem odložená, chovají se k ohni, ke dni soudu a zatracení bezbožných lidí.
\v 8 Ale tato jedna věc nebuď před vámi skryta, nejmilejší, že jeden den u Pána jest jako tisíc let, a tisíc let jako jeden den.
\v 9 Nemeškáť Pán s \add naplněním\add* slibů, (jakož někteří za to mají, že obmeškává,) ale shovívá nám, nechtě, aby kteří zahynuli, ale všickni ku pokání se obrátili.
\v 10 Přijdeť pak ten den Páně, jako zloděj v noci, v kterémž nebesa jako v prudkosti vichru pominou, a živlové pálivostí ohně rozplynou se, země pak i ty věci, kteréž jsou na ní, vypáleny budou.
\v 11 Poněvadž tedy to všecko má se rozplynouti, jací pak vy býti máte v svatých obcováních a v pobožnostech,
\v 12 Očekávajíce a chvátajíce ku příští dne Božího, v němžto nebesa, hoříce, rozpustí se, a živlové pálivostí ohně rozplynou se?
\v 13 Nového pak nebe a nové země podlé zaslíbení jeho čekáme, v kterýchž spravedlnost přebývá.
\v 14 Protož, nejmilejší, takových věcí čekajíce, snažtež se, abyste bez poškvrny a bez úhony před ním nalezeni byli v pokoji.
\v 15 A Pána našeho dlouhočekání za spasení mějte, jakž i milý bratr náš Pavel, podlé sobě dané moudrosti, psal vám,
\v 16 Jako i ve všech epištolách \add svých\add*, mluvě v nich o těch věcech. Mezi nimiž některé jsou nesnadné k vyrozumění, kterýchžto neučení a neutvrzení natahují, jako i jiných písem, k svému vlastnímu zatracení.
\v 17 Vy tedy, nejmilejší, \add to\add* prvé vědouce, střeztež se, abyste bludem těch nešlechetných nebyli pojati, a nevypadli od své pevnosti.
\v 18 Ale rozmáhejte se v milosti a v známosti Pána našeho a spasitele Jezukrista, jemuž sláva i nyní i na časy věčné. Amen.
