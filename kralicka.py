#!/usr/bin/env python3
import codecs
import collections
import re
import urllib.parse
import urllib.request

# Enter to http://cs.wikisource.org/wiki/Special:Export/
# Update deuterocanonical books
# http://crosswire.org/wiki/OSIS_Book_Abbreviations
WSbooks = collections.OrderedDict((
    ('Genesis', ('Gen', 'GEN',)),
    ('Exodus', ('Exod', 'EXO',)),
    ('Leviticus', ('Lev', 'LEV',)),
    ('Numeri', ('Num', 'NUM',)),
    ('Deuteronomium', ('Deut', 'DEU',)),
    ('Jozue', ('Josh', 'JOS',)),
    ('Soudců', ('Judg', 'JDG',)),
    ('Rut', ('Ruth', 'RUT',)),
    ('I Samuelova', ('1Sam', '1SA',)),
    ('II Samuelova', ('2Sam', '2SA',)),
    ('I Královská', ('1Kgs', '1KI',)),
    ('II Královská', ('2Kgs', '2KI',)),
    ('I Paralipomenon', ('1Chr', '1CH',)),
    ('II Paralipomenon', ('2Chr', '2CH',)),
    ('Ezdráš', ('Ezra', 'EZR',)),
    ('Nehemiáš', ('Neh', 'NEH',)),
    ('Ester', ('Esth', 'EST',)),
    ('Job', ('Job', 'JOB',)),
    ('Žalmy', ('Ps', 'PSA',)),
    ('Přísloví', ('Prov', 'PRO',)),
    ('Kazatel', ('Eccl', 'ECC',)),
    ('Píseň Šalamounova', ('Song', 'SNG',)),
    ('Izaiáš', ('Isa', 'ISA',)),
    ('Jeremiáš', ('Jer', 'JER',)),
    ('Pláč Jeremiášův', ('Lam', 'LAM',)),
    ('Ezechiel', ('Ezek', 'EZK',)),
    ('Daniel', ('Dan', 'DAN',)),
    ('Ozeáš', ('Hos', 'HOS',)),
    ('Joel', ('Joel', 'JOL',)),
    ('Amos', ('Amos', 'AMO',)),
    ('Abdiáš', ('Obad', 'OBA',)),
    ('Jonáš', ('Jonah', 'JON',)),
    ('Micheáš', ('Mic', 'MIC',)),
    ('Nahum', ('Nah', 'NAM',)),
    ('Abakuk', ('Hab', 'HAB',)),
    ('Sofoniáš', ('Zeph', 'ZEP',)),
    ('Aggeus', ('Hag', 'HAG',)),
    ('Zachariáš', ('Zech', 'ZEC',)),
    ('Malachiáš', ('Mal', 'MAL',)),
    # Only when all DT book available
    # ('Modlitba Manassesova', ('PrMan', 'MAN',)),
    # ('Tobíáš', ('Tob', 'TOB',)),
    # Not yet ('První kniha Machabejská', ('1Macc', '1MA',)),
    # ('Judit', ('Jdt', 'JDT',)),
    # ('Báruch', ('Bar', 'BAR',)),
    # ('Přídavkové k proroctví Danielovu', ('AddDan', '???',)),
    # ('První kniha Ezdrášova', ('1Esd', '1ES',)),
    # ('Druhá kniha Ezdrášova', ('2Esd', '2ES',)),
    # ('Přídavkové ke knize Ester', ('AddEsth', 'ADE',)),
    # ('Druhá kniha Machabejská', ('2Macc', '2MA',)),
    # ('Třetí kniha Machabejská', ('3Macc', '3MA',)),
    # ('Kniha Moudrosti', ('Wis', 'WIS',)),
    # ('Kniha Sírachova', ('Sir', 'SIR',)),
    ('Matouš', ('Matt', 'MAT',)),
    ('Marek', ('Mark', 'MRK',)),
    ('Lukáš', ('Luke', 'LUK',)),
    ('Jan', ('John', 'JHN',)),
    ('Skutky', ('Acts', 'ACT',)),
    ('Římanům', ('Rom', 'ROM',)),
    ('I Korintským', ('1Cor', '1CO',)),
    ('II Korintským', ('2Cor', '2CO',)),
    ('Galatským', ('Gal', 'GAL',)),
    ('Efezským', ('Eph', 'EPH',)),
    ('Filipenským', ('Phil', 'PHP',)),
    ('Kolossenským', ('Col', 'COL',)),
    ('I Tessalonicenským', ('1Thes', '1TH',)),
    ('II Tessalonicenským', ('2Thes', '2TH',)),
    ('I Timoteovi', ('1Tim', '1TI',)),
    ('II Timoteovi', ('2Tim', '2TI',)),
    ('Titovi', ('Titus', 'TIT',)),
    ('Filemonovi', ('Phlm', 'PHM',)),
    ('Židům', ('Heb', 'HEB',)),
    ('Jakub', ('Jas', 'JAS',)),
    ('I Petrova', ('1Pet', '1PE',)),
    ('II Petrova', ('2Pet', '2PE',)),
    ('I Janova', ('1John', '1JN',)),
    ('II Janova', ('2John', '2JN',)),
    ('III Janova', ('3John', '3JN',)),
    ('Judova', ('Jude', 'JUD',)),
    ('Zjevení', ('Rev', 'REV',)),
))

"""
articleList = ''
for WSbook in WSbooks:
    articleList += WSbase + '/' + WSbook + '\n'

"""

OSISdoc = ''
WSbase = 'Bible_kralická_(1918)/'
for krBook in WSbooks:
    print(krBook)
    url = 'https://cs.wikisource.org/wiki/Special:Export'
    vals = {'pages': (WSbase + krBook).encode('utf8'),
            'curonly': 1,
            'wpDownload': 0}
    vals = urllib.parse.urlencode(vals)
    request = urllib.request.Request(url, vals.encode('utf-8'))
    inputDoc = urllib.request.urlopen(request).read().decode('utf-8')

    inputDoc = re.sub(r'.*<text .+?>(.+?)</text>.*', r'\1', inputDoc,
                      flags=re.S | re.I)

    inputDoc = re.sub('  +', ' ', inputDoc)

    inputDoc = re.sub(r'{{header.+?}}', '', inputDoc, flags=re.S | re.I)
    inputDoc = re.sub(r'{{RIGHTTOC}}', '', inputDoc)
    inputDoc = re.sub(r'{{Redakční poznámky}}', '', inputDoc)
    inputDoc = re.sub(r'{{NavigacePaP.+?}}', '', inputDoc,
                      flags=re.S | re.I)
    inputDoc = re.sub(r'^{{Textinfo.+?^}}', '', inputDoc,
                      flags=re.S | re.M | re.I)
    inputDoc = re.sub(r'{{Rozšířit}}.+?}}', '', inputDoc,
                      flags=re.S | re.I)
    inputDoc = re.sub(r'{{Other versions.+?}}', '', inputDoc,
                      flags=re.S | re.I)
    inputDoc = re.sub(r'{{biblickýobsah.+?}}.*', '', inputDoc,
                      flags=re.S | re.I)
    inputDoc = re.sub(r'\[\[Kategorie.+?\]\]', '', inputDoc,
                      flags=re.S | re.I)
    inputDoc = re.sub(r'&lt;section .+?&gt;', '', inputDoc,
                      flags=re.S | re.I)
    inputDoc = re.sub(r'&lt;onlyinclude&gt;{{{.+?\|\s*(.+?)}}}' +
                      r'&lt;/onlyinclude&gt;', r'\1',
                      inputDoc, flags=re.S | re.I)
    inputDoc = re.sub(r'&lt;/?onlyinclude&gt; *', '', inputDoc,
                      flags=re.S | re.I)

    inputDoc = re.sub(r'== Kapitola \d+\. ==', '', inputDoc, flags=re.I)
    inputDoc = re.sub(r'{{kapitola\|(\d+)}}\s*', r'\\c \1\n',
                      inputDoc, flags=re.I)
    inputDoc = re.sub(r'{{verš\|kapitola=\d+\|verš=(\d+)}}\s*', r'\\v \1 ',
                      inputDoc, flags=re.I)

    inputDoc = re.sub(r'==External links==.+', '', inputDoc,
                      flags=re.S | re.I)

    inputDoc = re.sub(r'== Žalm (\d+?)\. ==\s*\n\\c (\d+)',
                      r'\\c \2\n\\s1 Žalm \1', inputDoc)

    inputDoc = re.sub(r'\[Note:(.+?) +\]', r'\\f + \1\\f*', inputDoc,
                      flags=re.S | re.I)
    inputDoc = re.sub(r'\[(.+?)\]', r'\\add \1\\add*', inputDoc,
                      flags=re.S | re.I)

    inputDoc = re.sub(r'&lt;div .+&lt;/div&gt;', '', inputDoc,
                      flags=re.S | re.I)

    USFMdoc = codecs.open('kralicka_' +
                          str(list(WSbooks.keys()).index(krBook)+1).zfill(2) +
                          '_'+WSbooks[krBook][1]+'.usfm', 'w', 'utf-8')

    USFMdoc.write('\id ' + WSbooks[krBook][1] + '\n')
    USFMdoc.write('\mt1 ' + krBook + '\n')

    for l in inputDoc.split('\n'):
        l = l.strip()
        if l:
            USFMdoc.write(l)
            USFMdoc.write('\n')
